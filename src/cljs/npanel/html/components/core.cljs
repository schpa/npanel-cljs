(ns npanel.html.components.core
  (:require [npanel.data.material.core]))

(defn id-as-string [key data]
  (apply str (reverse (key @data))))

(defn click-fn'' [{:keys [data key completion-code-fn back-fn card-id-length card-password-length]} x]
  (condp contains? (str x)
    #{"V"} (completion-code-fn data (id-as-string key data))
    #{"B"} (back-fn data key)
    (set (map str (range 10))) (if (< (count (key @data)) (+ card-id-length card-password-length))
                                 (swap! data assoc key (cons x (key @data))))
    (println (str x) (.-target x))))

(defn pad [{:keys [data key click-fn back-fn validate-fn completion-code-fn] :as args} e]
  (let [[t1 t2] (condp contains? (str e)
                  #{"B"} [:button.pad.centered [:span.icon.i.fas.fa-backspace {:style {:height "3rem" :pointer-events 'none}}]]
                  #{"V"} [:button.pad.has-text-success.centered [:span.icon.i.fas.fa-check-circle {:style {:height "3rem" :pointer-events 'none}}]]
                  (set (map str (range 10))) [:button.pad e]
                  (do (println "nk.components.keypad/pad: condp no-match " e) [nil nil]))]

    (do
      (println "EE " e " " (contains? #{'V} e))
      (if (not (or (nil? t1) (nil? t2)))
        [t1 {:key (str "pad" e)
             :disabled (condp contains? (str e)
                         #{"B"} (empty? (key @data))
                         #{"V"} (not (validate-fn key data))
                         (into #{} (map str (range 10))) (validate-fn key data)
                         false)
             :id e
             :on-click (fn [e]
                         (println "on-click: " (js->clj e))
                         (.preventDefault e)
                         (->> e .-target.id click-fn))}
         t2]
        (do (println "Did not handle") [:div "nada"])))))