(ns npanel.html.components.member.core
  (:require [npanel.html.components.core :refer [click-fn'' pad]]))

(defn keypad-user [{:keys [completion-code-fn complete-cardid-fn validate-fn card-id-length card-password-length data key] :as args}]
  (fn []
    (let [title (id-as-string key data)]
      [:div.two-content {:style {:background 'black}}
       [:div.xxxx.readwrite
        (map-indexed (fn [i e] ^{:key (str "a" i e)} [:div.c e]) (take 5 title))
        (map-indexed (fn [i e] ^{:key (str "b" i e)} [:div.ce " "]) (range (- 5 (count (take 5 title)))))
        [:div.cs "–"]
        (map-indexed (fn [i e] ^{:key (str "c" i e)} [:div.ch [:span.icon.i.fas.fa-key]]) (take 2 (drop 5 title)))
        (map-indexed (fn [i e] ^{:key (str "d" i e)} [:div.ceh [:span.icon.i.fas.fa-key]]) (range (- 2 (count (take 2 (drop 5 title))))))]

       (if (validate-fn key data)
         [:div.instructions.titlev2.center "trykk ok"]
         (if (< (count (key @data)) card-id-length)
           [:div.instructions.titlev2.centered "Hva er medlemsnummeret ditt?"]
           (if (complete-cardid-fn (subs title 0 card-id-length))
             [:div.instructions.titlev2.centered "Hva er koden? "]
             [:div.instructions.titlev2.centered "Finner ikke medlem"])))

       [:div.keypad {:style {:width "100%" :height "100%"}}
        (doall (map (partial ^{:key (str "pad" e)} pad (assoc args :click-fn (partial click-fn'' args))) '(7 8 9 4 5 6 1 2 3 V 0 B)))]])))
