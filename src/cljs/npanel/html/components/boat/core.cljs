(ns npanel.html.components.boat.core
  (:require [clojure.string :refer [capitalize]]            ;remove
            [npanel.data.material.measurement :as measurement] ;remove
            [npanel.data.material.weight :as weight]        ;remove
            [npanel.html.components.symbols :as symbols]    ;remove
            [npanel.data.material.boat.core :as boat]
            [npanel.html.components.core :refer [click-fn'' pad]]))

(defn -detailed [desc]
  (fn []
    (if-not (nil? desc)
      [:div.boat-description
       [:div.horiz-boxes
        [:div.item
         [:div.horiz-boxes
          [:div.item
           [:p.titlev2 (str desc.serial " " (capitalize desc.type))]
           [:p desc.brand]]]]
        [:div.horiz-boxes
         [:div.item {:style {:padding-right "1rem"}}
          [:p {:style {:text-align 'right}} desc.width]
          [:p {:style {:text-align 'right}} desc.length]]
         [:div.centered {:style {:heights "3rem" :width "3rem"}} desc.symbol]]]
       [:p.weight-p "Passer personer med vekt " desc.weight]
       [:p.description-p desc.description]]
      [:div (str desc)])))

(defn -minimal [desc]
  (fn []
    (if-not (nil? desc)
      [:div.boat-description
       [:div.horiz-boxes
        [:div.item
         [:div.horiz-boxes
          [:div.item
           [:p.titlev2 (str desc.serial " " (capitalize desc.type))]
           [:p desc.brand]]]]
        [:div.horiz-boxes
         [:div.item {:style {:padding-right "1rem"}}
          [:p {:style {:text-align 'right}} desc.width]
          [:p {:style {:text-align 'right}} desc.length]]
         [:div.centered {:style {:heights "3rem" :width "3rem"}} desc.symbol]]]
       [:p.weight-p "Passer personer med vekt " desc.weight]]
      [:div (str desc)])))


;; note: experimental


(defprotocol As-component
  (detailed [this])
  (minimal [this]))

(extend-type boat/Description
  As-component
  (detailed [this]
    (-detailed this))
  (minimal [this]
    (-minimal this)))

;; ---


(defn description-component [serial]
  {:pre [(string? serial)]}
  (fn []
    (if (boat/valid-id? serial)
      (let [type (:type (get boat/root-map (js/parseInt serial)))
            material (:material (get boat/boat-map type))
            level (:level2 (get boat/boat-map type))]
        [:div.instructions
         [:div.horiz-boxes
          [:div.item
           [:div.horiz-boxes
            [:div.centered {:style {:heights "3rem" :width "3rem"}} (symbols/svg-circle-level material level)]
            [:div.item {:style {:padding-left "12px"}}
             [:div.titlev2 (str serial " " (capitalize (boat/base-category-map (:base-category (boat/boat-map type)))))]
             [:div (:name (boat/boat-map type))]]]]

          (let [d (measurement/parse-dimension-v2 (:dimensions (boat/boat-map type)))]
            (when (= :ok (:status d))
              (let [w (:width (:value d))
                    l (:length (:value d))]
                [:div.item
                 [:div {:style {:text-align 'right}} (when w (capitalize (str "bredde " w " cm")))]
                 [:div {:style {:text-align 'right}} (when l (capitalize (str "lengde " l " cm")))]])))]

         (let [w (weight/string-for-weight-class (:weight-class (boat/boat-map type)))]
           (when (= :ok (:status w))
             [:div "Passer personer med vekt " (:value w)]))

         [:div (measurement/description-from-type boat/description-map type)]])
      (if (= 3 (count serial))
        [:div.instructions.titlev2.centered (str "Finner ikke noe registrert på " serial)]
        (let [digit-count (count serial)]
          [:div.name (str "Trenger " (- 3 digit-count) " siffer" (if (<= 1 digit-count) " til"))])))))

(defn keypad-boat [{:keys [validate-fn card-id-length data key] :as args}]
  (fn []
    (let [serial (boat/id-as-string key data)]
      [:div.two-content {:style {:background 'black}}
       [:div.xxxx.readwrite
        (map-indexed (fn [i e] ^{:key (str "a" i e)} [:div.c e]) (take card-id-length serial))
        (map-indexed (fn [i e] ^{:key (str "b" i e)} [:div.ce " "]) (range (- card-id-length (count (take card-id-length serial)))))]

       (if (validate-fn key data)
         [(detailed (boat/description-for serial))]
         (if (< (count serial) card-id-length)
           [:div.instructions.titlev2.centered "Hva er båtnummeret?"]
           [:div.instructions.titlev2.centered (str "Ingen passer, har du fått riktig nummer?")]))

       [:div.keypad {:style {:width "100%" :height "100%"}}
        (doall (map (partial ^{:key (str "pad" e)} pad (assoc args :click-fn (partial click-fn'' args))) '(7 8 9 4 5 6 1 2 3 0 B V)))]])))
