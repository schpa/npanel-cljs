(ns npanel.html.components.symbols)

(defn dotted-horizontal-line []
  (when false
    [:path {:d "M 0 50 l 200 0 " :style {:stroke-dasharray "1,2" :stroke-width 2 :stroke "red" :fill "none"}}]))

(defn dotted-square []
  (when false
    [:rect {:x 0 :y 0 :width "100%" :height 100 :stroke-dasharray "2,2" :fill "none" :stroke "black"}]))

(defn adjust-w-opticals [base n]
  "The opticals for 1 looks a bit offset"
  (- base (condp = n
            0 2
            0)))

(defn- material-color [m]
  (condp = m
    0 "green"
    1 "blue"
    "black"))

(defn svg-circle-level [material level]
  (let [bg-color (material-color material)
        text-color "white"
        border-color (cond (= level :?) "gray" :else "none")
        text (if (= level :?) "?"  (inc level))]
    [:svg {:viewBox "0 0 100 100" :height "100%" :width "100%"}
     [:circle {:cx 50 :cy 50 :r 48 :style {:stroke-width 1 :stroke border-color :fill bg-color}}]
     [:text {:text-anchor       "middle" :x (adjust-w-opticals 50 level) :y 56
             :dominant-baseline "middle"
             :style             {:font-size 80 :font-weight 900 :fill text-color}} text
      [dotted-square]
      (dotted-horizontal-line)]]))