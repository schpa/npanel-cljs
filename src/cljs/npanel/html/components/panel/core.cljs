(ns npanel.html.components.panel.core
  (:require [npanel.html.style.core :as s]
            [npanel.html.state :as state]
            [cljs-time.core :refer [now date-time]]
            [npanel.reducer.core :as reducer]))

(defn statusbar []
  (fn [st _]
    [:div
     (s/reset-state-button st)
     ;; think: hard one
     ;; todo: refactor later
     [:button.button.is-warning.m-b-md {:on-click (fn [] (swap! st update :activity #(:data (reducer/reduce-events % [{:user/id 1 :user/activity :out :boat/id 900 :time (now)}]))))} "leie"]
     [s/statusbar
      (if (= :some/check-in (@st :some/ui-state))
        (s/button-active st :some/check-in "inn-")
        (s/button st :some/check-in "leie"))

      (s/tripple-click-button
       (count (filter (comp :out val) (@st :activity)))
       :disabled (@st :some/ui-state)
       :click-fn (fn [] (swap! st assoc :current-activity-viewing :boats-out)))

      (s/cycle-button st "sorting")

      (s/tripple-click-button
       (count (filter (comp not :out val) (@st :activity)))
       :disabled (@st :some/ui-state)
       :click-fn (fn [] (swap! st assoc :current-activity-viewing :boats-in)))

      (if (= :some/check-out (@st :some/ui-state))
        (s/button-active st :some/check-out "ut-")
        (s/button st :some/check-out "levere"))]]))

(defn activity []
  (fn [st _]
    (let [timestamp (fn [minute] (date-time 2018 1 1 11 minute 0))
          initial-model {}
          model-agg (:data (reducer/reduce-events
                            initial-model
                             ;; events
                            [{:user/id 1
                              :user/activity :out
                              :boat/id 100
                              :time (timestamp 0)}
                             {:user/id 2
                              :user/activity :out
                              :boat/id 200
                              :time (timestamp 12)}
                             {:user/id 3
                              :user/activity :out
                              :boat/id 300
                              :time (timestamp 23)}
                             {:user/id 3
                              :user/activity :out
                              :boat/id 400
                              :time (timestamp 90)}
                             {:user/id 2
                              :user/activity :in
                              :boat/id 200
                              :time (timestamp 90)}]))
          f (fn [e]
              [(if (:out (val e))
                 :div.one-middle.card-id-owner-adult.boat-out
                 :div.one-middle.card-id-owner-adult.boat-in)
               [:p (str (key e) " - " (:out (val e)))]
               [:p (:time (val e))]
               [:p (:user/id (val e))]])]
      [:div.fullscreen-landscape-ipad-content
       [:div.fullscreen-landscape-ipad-content-both
        [:div.one-middle-list.one-middle
         ;;think: comp(ose) is a shortcut for taking out the val of the mapentry and then checking if the :out field of that is true
         (condp = (:current-activity-viewing @st)
           :boats-out (map f (if (= 0 (:activity-view/sort-order @st))
                               (sort-by :time (filter (comp :out val) (:activity @st)))
                               (reverse (sort-by :time (filter (comp :out val) (:activity @st))))))
           :boats-in (map f (filter (comp not :out val) (:activity @st)))
           (map f (:activity @st)))]]])))

         ;;(filter (comp :out val) model-agg)

(comment
  (reducer/reduce-events {} (:activity @npanel.C-panel-elements/state))
  (let [timestamp (fn [minute] (date-time 2018 1 1 11 minute 0))
        model-agg (:data (reducer/reduce-events
                          {}
                          [{:user/id 1
                            :user/activity :out
                            :boat/id 100
                            :time (timestamp 0)}
                           {:user/id 2
                            :user/activity :out
                            :boat/id 200
                            :time (timestamp 12)}
                           {:user/id 3
                            :user/activity :out
                            :boat/id 300
                            :time (timestamp 23)}
                           {:user/id 3
                            :user/activity :out
                            :boat/id 400
                            :time (timestamp 90)}
                           {:user/id 2
                            :user/activity :in
                            :boat/id 200
                            :time (timestamp 90)}]))]
    model-agg))
