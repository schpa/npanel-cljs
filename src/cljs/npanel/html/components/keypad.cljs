(ns npanel.html.components.keypad
  (:require [clojure.string :refer [capitalize]]
            [npanel.html.components.core]
            [npanel.data.material.boat.core]
            [npanel.data.material.weight :as weight]
            [npanel.data.material.measurement :as measurement]
            [npanel.html.components.symbols :as symbols]))
