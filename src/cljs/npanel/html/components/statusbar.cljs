(ns npanel.html.components.statusbar
  (:require [reagent.core :as r :refer [atom]]
            [npanel.html.components.symbols]
            [npanel.data.member.core  :as member]
            [cljs-time.core :as t]
            [clojure.set :as set]))

(def clicked
  (fn [f e]
    (do (println "clicked " (->> e .-target.id))
        (f))))

(defn sign-in-button-enabled [data]
  (= 1 (:state @data)))

(defn header-bar [data]
  (fn []
    [:div.fullscreen-landscape-ipad-statusbar
     [:div.statusbar-item2.centered {:class [(when (= :rent-boat (:state @data)) 'selected)
                                             (when (= :deliver-boat (:state @data)) 'dimmed)]

                                     :on-click (fn [] (swap! data assoc :state :rent-boat))} "Låne ut"]
     [:div.statusbar-item2.centered.dimmed]
     [:div.statusbar-item2.centered.dimmed]
     [:div.statusbar-item2 {:class [(when (=  :deliver-boat (:state @data)) 'selected)
                                    (when (= :rent-boat (:state @data)) 'dimmed)]
                            :on-click (fn [] (swap! data assoc :state :deliver-boat))} "Levere inn"]]))

(defn activity-content-footer-bar [data]
  (fn []
    (condp = (:state @data)
      :default [:div.fullscreen-landscape-ipad-statusbar
                [:div.statusbar-item2.centered
                 {:on-click (fn [] (swap! data assoc
                                          :state :default1
                                          :top-state 0))
                  :class ""} "Aktivitet"]
                [:div.statusbar-item2.centered.dimmed "Historikk"]
                [:div.statusbar-item2.dimmed ""]
                [:div.statusbar-item2.centered {:on-click (fn [] (swap! data assoc :state :default2))} "Nøkkelvakt"]]
      [:div])))

(defn- selected [data e] (when (= e (:activity-view @data)) 'selected))
(defn- do-click [data e] (fn [] (swap! data assoc :activity-view e)))
(defn- status-button [data key caption] [:div.statusbar-item2.centered
                                         {:on-click (do-click data key)
                                          :class (selected data key)} caption])
(defn footer-bar [data]
  (fn []
    (condp = (:state @data)
      :default [:div.fullscreen-landscape-ipad-statusbar
                (status-button data :boats-out "Båter ute nå")
                (status-button data :all-today "Innleverte i dag")
                (status-button data :report "Rapporter")
                (status-button data :special "Nøkkelvakt")]

      :rent-boat [:div.fullscreen-landscape-ipad-statusbar
                  [:div.statusbar-item2.centered
                   {:on-click (fn [] (swap! data assoc :state :default))} "Avbryt"]
                  [:div.statusbar-item2.centered.dimmed "Historikk"]
                  [:div.statusbar-item2.dimmed ""]
                  [:div.statusbar-item2.centered {:on-click (fn [] (swap! data assoc
                                                                          :state :default
                                                                          :active-user nil
                                                                          :members-signed-in nil
                                                                          :digits nil))} "Ferdig"]]

      :deliver-boat [:div.fullscreen-landscape-ipad-statusbar
                     [:div.statusbar-item2.centered
                      {:on-click (fn [] (swap! data assoc :state :default))} "Avbryt"]
                     [:div.statusbar-item2.centered.dimmed "Historikk"]
                     [:div.statusbar-item2.dimmed ""]
                     [:div.statusbar-item2.centered {:on-click (fn [] (swap! data assoc
                                                                             :state :default
                                                                             :active-user nil
                                                                             :members-signed-in nil
                                                                             :digits nil))} "Ferdig"]]

      [:div])))