(ns npanel.html.table)

(defn click-fn [row key]
  ;(swap! value assoc key e))
  (println (str row key)))

(defn basic-table
  ([source cols col-fn]
   (if (= PersistentArrayMap (type cols))
     (let [header-keys (keys cols)
           header-vals (vals cols)
           row-fn (fn [row] (map (fn [key] [:td (col-fn key row)]) header-keys))]
       [:table.table.is-fullwidth.is-hoverable.is-striped
        [:thead [:tr (map (fn [e] [:th (clojure.string/capitalize (name e))]) header-vals)]]
        [:tbody (map (fn [e] [:tr (row-fn e)]) source)]])

     (let [row-fn (fn [row] (map (fn [key] [:td (col-fn key row)]) cols))]
       [:table.table
        [:thead [:tr (map (fn [e] [:th (clojure.string/capitalize (name e))]) cols)]]
        [:tbody (map (fn [e] [:tr (row-fn e)]) source)]])))

  ([source cols]
   (if (= PersistentArrayMap (type cols))
     (let [header-keys (keys cols)
           header-vals (vals cols)
           row-fn (fn [row] (map (fn [key] [:td {:on-click (fn [e] (click-fn row key))} (key row)]) header-keys))]
       [:table.table.is-fullwidth.is-hoverable.is-striped
        [:thead [:tr (map (fn [e] [:th (clojure.string/capitalize (name e))]) header-vals)]]
        [:tbody (map (fn [e] [:tr (row-fn e)]) source)]])

     (let [row-fn (fn [row] (map (fn [key] [:td (str (key row))]) cols))]
       [:table.table
        [:thead [:tr (map (fn [e] [:th (clojure.string/capitalize (name e))]) cols)]]
        [:tbody (map (fn [e] [:tr (row-fn e)]) source)]]))))

(defn selectable-table  [source cols col-fn]
  (if (= PersistentArrayMap (type cols))
    (let [header-keys (keys cols)
          header-vals (vals cols)
          row-fn (fn [row] (map (fn [key] [:td {:on-click (fn [_] (col-fn key row))} (key row)]) header-keys))]
      [:table.table.is-fullwidth.is-striped
       [:thead [:tr (map (fn [e] [:th (clojure.string/capitalize (name e))]) header-vals)]]
       [:tbody (map (fn [row] [:tr (row-fn row)])
                    source)]])))