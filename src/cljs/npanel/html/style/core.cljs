(ns npanel.html.style.core
  (:require [npanel.html.state :as state]))

(def statusbar :div.fullscreen-landscape-ipad-statusbar)

(defn button [st key caption] [:button.statusbar-item2
                               {:on-click (fn [] (state/toggle-next st key))
                                :style {:border 'none
                                        :color 'white}} caption])

(defn button-active [st key caption] [:button.statusbar-active-item2
                                      {:on-click (fn [] (state/toggle-next st key))
                                       :style {:border 'none
                                               :color 'white}} caption])

(defn tripple-click-button [caption & {:keys [disabled click-fn]}]
  [:button.statusbar-item2 {:on-click click-fn
                            :style {:border 'none
                                    :background :#333

                                    :color (if disabled :#444 'white)}} caption])

(defn reset-state-button [st]
  [:button.button.m-b-md {:on-click (fn [] (reset! st {}))} "reset state"])

(defn cycle-button [st caption]
  [:button.statusbar-item2 {:on-click (fn [] (state/toggle-next st :activity-view/sort-order))
                            :style {:border 'none :background :#222 :color (if (@st :some/ui-state) :#444 'white)}} (str caption " ") (state/get-st st :activity-view/sort-order)])