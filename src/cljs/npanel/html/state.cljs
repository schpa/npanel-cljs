(ns npanel.html.state
  (:require [reagent.core :as r]))

(def state
  (r/atom {:ui-state []
           :activity []}))

(defn toggle-next [st which]
  (condp = which
    :some/view-state (swap! st update which (if (> 2 (which @st)) inc (fn [] 0)))
    :some/check-in (swap! st assoc :some/ui-state :some/check-in)
    :some/check-out (swap! st assoc :some/ui-state :some/check-out)
    :activity-view/sort-order (swap! st update which (if (> 1 (which @st)) inc (fn [] 0)))))

;;note: for saner error-messages when missing clauses
(defn get-st [st which]
  (condp = which
    :some/view-state (which @st)
    :activity-view/sort-order (which @st)))