(ns npanel.data.material.core)

(defn direction-to-degrees [dir]
  (condp = dir
    :up 180
    :left 90
    :right 270
    :down 0
    0))

(def handling-map
  {0 {:handling :green :level 0}
   1 {:handling :green :level 1}
   2 {:handling :green :level 2}
   3 {:handling :blue :level 0}
   4 {:handling :blue :level 1}
   5 {:handling :blue :level 2}
   6 {:handling :blue :level 3}
   7 {:handling :blue :level 4}})

(def category-map
  {0, "2-seters havkajakk",
   1, "Grønlandskajakk",
   2, "Havkajakk",
   3, "Havkajakk liten",
   4, "Havkajakk middels",
   5, "Havkajakk stor",
   6, "Junior havkajakk",
   7, "Junior treningsracer",
   8, "SUP",
   9, "Rekreasjonskajakk",
   10, "Rekreasjonskajakk for voksen med barn",
   11, "Sit-on-top kajakk",
   12, "Solokano",
   13, "Surfski",
   14, "Treningskajakk",
   15, "Treningsracer",
   16, "Kano 2-seters",
   17, "Kano 3-seters",
   18, "Skjærgårdskajakk"})
