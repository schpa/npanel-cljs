(ns npanel.data.material.weight
  (:require [clojure.string :refer [split]]))

(defn string-for-weight-class [k]
  "Takes a concise way of expressing weight-boundaries in
  keyword-form and returns a verbose (and friendly) representation.

  :120-    (passer best for personer fra) 120 kg og oppover
  :90-50   fra 50 til 90 kg
  :50-90   fra 50 til 90 kg
  :-90     opp til 90 kg

  :abc-123 :abc-abc :123-abc :etc *fails*
  "
  (if (or (= :? k) (nil? k) (empty? (name k)))
    ;; if there was no input, we won't give any meaningful output
    ;; todo(cps): rework this
    {:status :empty :value k}
    (let [[a b] (split (name k) "-")
          ;; check if format has meaningful parameters
          any-f? (fn [x] (or (empty? x) (not (js/isNaN x))))]
      (if (and (any-f? a) (any-f? b))
        (let [a' (js/parseInt a)
              b' (js/parseInt b)]
          {:status :ok
           :value (cond
                    (and (empty? a) b') (str "opptil " b " kg")
                    (and a' (empty? b)) (str "fra " a " kg og oppover")
                    (and a' b')
                    ;; sort and fix inverted ranges eg. :140-90
                    (let [[a b] (sort [a' b'])]
                      (str "mellom " a " og " b " kg")))})

        ;; illegal format
        {:status :error}))))

(defn fit-weight-text [c]
  (if c
    (str "passer for de " (string-for-weight-class c))
    "got nil"))