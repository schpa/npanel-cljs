(ns npanel.data.material.boat.core
  (:require [clojure.string :refer [capitalize]]
            [npanel.data.material.measurement :as measurement]
            [npanel.data.material.weight :as weight]
            [npanel.data.material.boat.access :as boat-access]
            [npanel.html.components.symbols :as symbols]))

;; defines the access-interface

(defn all [])

(declare
 description-for
 sample-description
 boat-map)

(defn valid-id? [id]
  (some? (boat-access/root-map (js/parseInt id))))

(defrecord Description [symbol serial type brand width length description weight])

(defn description-for [serial]
  (if (valid-id? serial)
    (let [type (:type (get boat-access/root-map (js/parseInt serial)))
          material (:material (get boat-map type))
          level (:level2 (get boat-map type))
          d (measurement/parse-dimension-v2 (:dimensions (boat-map type)))
          w (:width (:value d))
          l (:length (:value d))]
      (map->Description
       {:symbol (symbols/svg-circle-level material level)
        :serial (str serial)
        :type (capitalize (boat-access/base-category-map (:base-category (boat-map type))))
        :brand (:brand (boat-access/boat-map type))
        :width (when w (capitalize (str "bredde " w " cm")) "")
        :length (when l (capitalize (str "lengde " l " cm")) "")
        :weight (weight/string-for-weight-class (:weight-class (boat-map type)))
        :description (boat-access/description-map type)}))))

(def sample-description
  (map->Description
   {:symbol (symbols/svg-circle-level 1 0)
    :serial "999"
    :type (boat-access/base-category-map 1)
    :brand "Some very very long brandname"
    :width (capitalize (str "bredde " 50 " cm"))
    :length (capitalize (str "lengde " 420 " cm"))
    :weight (:value (weight/string-for-weight-class :120-409))
    :description (repeat 5 (str (boat-access/description-map 32) " " (boat-access/description-map 36)))}))

(def root-map boat-access/root-map)

(def boat-map boat-access/boat-map)

(def base-category-map boat-access/base-category-map)

(defn id-as-string [key data])

(def description-map boat-access/description-map)

(def count-base-category
  (frequencies (map (fn [e] (:base-category (get boat-map e))) (->> boat-access/root-map (map val) (map :type)))))

(defn- filter-where-serial-= [serial db]
  (filter (fn [e] (= serial (get e :serial))) db))

(defn root-map-lookup [type-id rawdata]
  (as-> type-id $
    (->> rawdata
         (filter (fn [e] (= $ (:type (val e)))))
         (map (fn [e] {(key e) (:aquired (val e))}))
         (into {}))))

(defn find-all-func []
  (->>
   (map key boat-map)
   (map (fn [e] {e (root-map-lookup e boat-access/root-map)}))
   (into {})))

(defn find-func [n]
  (get (find-all-func) n))

(defn- f [idx [serial type year-aquired]]
  (let [m (get boat-map type)]
    (assoc m :idx idx
           :key type
           :description (description-for type)
           :serial serial
           :dimensions (measurement/parse-dimension (:dimensions m))
           :weight-description (weight/fit-weight-text (:weight-class m))
           :year-aquired (str year-aquired))))

(def indexed-boat-map
  (->> (into [] (map (fn [e] [(key e) (:type (val e)) (:aquired (val e))]) boat-access/root-map))
       (map-indexed f)))

(def frequencies-of-brand-map
  (map (fn [[key freq]] {:key key :freq freq})
       (reverse (sort-by second (frequencies (->> boat-access/root-map (map val)))))))
