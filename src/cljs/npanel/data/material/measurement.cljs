(ns npanel.data.material.measurement
  (:require [clojure.string :refer [split includes?]]))

(def inch 2.54)

(def foot 30.48)

(defn feet-to-cm [f]
  (if (number? f)
    (let [n (js/parseFloat f)]
      (if (js/isNaN n)
        0
        (* n (* inch 12))))
    {:error f}))

(defn inch-to-cm [f]
  (if (number? f)
    (let [n (js/parseFloat f)]
      (if (js/isNaN n)
        0
        (* n inch)))
    {:error f}))

(defn parse-feet-inch-string [s]
  (let [r (->> (split s "'") (map js/parseFloat) vec)]
    (map (fn [e] (if (js/isNaN e) nil e)) r)))

(defn parse-feet [s]
  "Takes a feet'inch string and returns a vector of feet and inch"
  (let [[f i :as fi] (->> (split s "'") (map (comp (fn [n] (if (js/isNaN n) 0 n)) js/parseFloat)) vec)]
    (if (empty? fi)
      0
      (+ (* foot f) ((fnil * 0) i inch)))))

(defn parse-cm [s]
  (js/parseFloat s))

(defn parse [s]
  (if (empty? s)
    nil
    (if (includes? s "'")
      (parse-feet s)
      (parse-cm s))))

(defn convert [t]
  "takes a vector of feet and inch and returns a map of {:cm :us {:feet :inch}}"
  {:cm (reduce + t) :us {:feet (/ (nth t 0) foot) :inch (/ (nth t 1) inch)}})

(defn converter [s]
  (let [s (if (keyword? s) (name s) s)
        [a b] (split s "|")]
    [(parse a) (parse b)]))

(defn parse-dimension [tag]
  (let [tag (name tag)
        [a b :as ab] (split tag "|")]
    {:width (parse a) :length (parse b)}))

(defn parse-dimension-v2 [tag]
  (if (empty? (name tag))
    {:status :error}

    (let [tag (name tag)
          [a b :as ab] (split tag "|")
          w (parse a)
          l (parse b)
          v (cond-> {}
              (and (not (js/isNaN (js/parseFloat w))) w) (assoc :width w)
              (and (not (js/isNaN (js/parseFloat l))) l) (assoc :length l))]
      (if (empty? v)
        {:status :error}
        {:status :ok :value v}))))

(defn description-from-type [source-map id]
  (get source-map id (str "description for entry " id " not found")))
