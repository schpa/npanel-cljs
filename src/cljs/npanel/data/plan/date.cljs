(ns npanel.data.plan.date
  (:require [cljs-time.format :as f]
            [cljs-time.periodic :as p]
            [cljs-time.coerce :refer [from-long to-long]]
            [cljs-time.core :as t :refer [year date-time now]]
            [reagent.core :as r]))

(def days-in-week
  [:Mandag
   :Tirsdag
   :Onsdag
   :Torsdag
   :Fredag
   :Lørdag
   :Søndag])

(defn day-name
  [e]
  (name (nth days-in-week
             (dec e))))

(defn month-name
  [m]
  (name (nth '[Januar
               Februar
               Mars
               April
               Mai
               Juni
               Juli
               August
               September
               Oktober
               November
               Desember]
             (dec m))))

(defn day-name-short
  [e]
  (apply str (take 2 (day-name e))))