(ns npanel.data.member.access)

(def all
  [{:id 90012 :navn "Janne Pan" :født-år 1982 :telefon "10203041" :kode "12" :relatert-id nil}
   {:id 90011 :navn "Petter Pan" :født-år 1987 :telefon "10203040" :kode "11" :relatert-id 90012}
   {:id 90013 :navn "Nils Pan" :født-år 2007 :telefon "10203043" :kode "13" :relatert-id 90012}
   {:id 90014 :navn "Kari Pan" :født-år 2010 :telefon "10203044" :kode "462" :relatert-id 90012}
   {:id 90010 :navn "Bob Hund" :født-år 2013 :telefon "10203044" :kode "245" :relatert-id 90012}
   {:id 90015 :navn "Arne Bo" :født-år 2008 :telefon "10203043" :kode "462" :relatert-id nil}
   {:id 90016 :navn "Gunnhild Hansen" :født-år 1963 :telefon "10203041" :kode "123" :relatert-id 90015}
   {:id 90017 :navn "Fam Pling" :født-år 1950 :telefon "10203044" :kode "11" :relatert-id nil}
   {:id 80021 :navn "Ole Bull" :født-år 1960 :telefon "10203044" :kode "11" :relatert-id nil}
   {:id 80022 :navn "Jan Bang" :født-år 1970 :telefon "10203044" :kode "11" :relatert-id nil}
   {:id 80023 :navn "Anne Stene" :født-år 1980 :telefon "10203044" :kode "11" :relatert-id nil}
   {:id 80024 :navn "Sofie Bang" :født-år 1990 :telefon "10203044" :kode "11" :relatert-id nil}
   {:id 80025 :navn "Pling Plong" :født-år 2000 :telefon "10203044" :kode "11" :relatert-id nil}])
