(ns npanel.data.member.core
  (:require [clojure.string :refer [split]]
            [cljs-time.core :as t]
            [npanel.data.member.access :as members]))

(defn firstname [id]
  (first (split (:navn (first (filter #(= (:id %) id) members/all))) " ")))

(defn initials [id]
  (apply str (map first (split (:navn (first (filter #(= (:id %) id) members/all))) " "))))

(defn adult? [id]
  (<= (- (t/year (t/now)) 12)  (:født-år (first (filter #(= (:id %) id) members/all)))))

(defn lookup-cardid [id]
  (first (filter #(= id (str (% :id))) members/all)))

;note: there are some holes in this impl.
;todo: add test
(defn get-all-related-data-from [medlemmer id]
  (if-let [f (first (filter #(= id (:id %)) medlemmer))]
    (->> (if (nil? (:relatert-id f))
           (let [r (map (juxt :id :kode) (filter #(= id (:relatert-id %)) medlemmer))]
             (concat r [((juxt :id :kode) f)]))
           (let [r (map (juxt :id :kode) (filter #(= (:relatert-id %) (:relatert-id f)) medlemmer))]
             (concat [((juxt :relatert-id :kode) f)] r)))
         (map (fn [e] {:id (first e) :kode (second e)})))
    nil))