(ns npanel.reducer.core
  (:require [reagent.core :as r]
            [cljs-time.core :as t :refer [date-time minus plus minutes now]]
            [cljs-time.format :as f :refer [parse formatter unparse]]
            [clojure.pprint :refer [pprint]]
            [clojure.set :as set]
            [npanel.data.material.boat.access :as boat]
            [devcards.core :as dc :refer [defcard-rg defcard-doc]])

  (:require-macros [devcards.core :as dc :refer [defcard defcard-doc defcard-rg deftest]]))

(def timestamp (t/date-time 2018 1 1 11 0 0))

(def demo-events
  [[90011 :out 246]
   [90012 :out 471]
   [90012 :out 426]
   [90014 :out 427]
   [90014 :in 427]
   [90015 :out 465]
   [90016 :in 465]
   [90013 :out 222]
   [90010 :out 465]
   [90010 :out 140]
   [80021 :out 154]
   [80022 :out 169]
   [80023 :out 180]
   [80023 :in 180]
   [80024 :out 147]
   [80025 :out 179]
   [80025 :in 179]])

(def root-map-set
  (set (keys boat/root-map)))

(defn- simple-time [time]
  (unparse (:hour-minute-second-ms f/formatters) time))

(defn- helper [err type msg time]
  (conj err {:time (simple-time time) type msg}))

(defn- log-normal-out [err id time]
  (helper err :ok (str "boat " id " marked out") time))

(defn- log-normal-in [err id time]
  (helper err :ok (str "boat " id " got home") time))

(defn- log-err-never-out [err id time]
  (helper err :err (str "boat " id " was never out") time))

(defn- log-err-already-out [err id time]
  (helper err :err (str "boat " id " was already out ") time))

(defn- log-unhandled [err e time]
  (helper err :err (str "unhandled event" e) time))

(defn- log-other [err e time]
  (helper err :err e time))

(defn reduce-events [state events]
  "
  Takes a list of events and returns an aggregated model. I need to take care of
  error handling somehow. Here I've captured all events in a log you can
  inspect later. 
  "
  (let [;todo perform extraction of userdata in 'register-in' and store it elsewhere
        register-in (fn [ag boat-id _] (assoc-in ag [boat-id :out] false))
        register-out (fn [ag boat-id time user-id]
                       (assoc-in ag [boat-id]
                                 {:out true
                                  :usage (+ 1 (get-in ag [boat-id :usage]))
                                  :time (unparse (:hour-minute f/formatters) time)
                                  :user/id user-id}))]
    ;recursively consume events and aggregate a state
    (loop [agg state event events err []]
      (if-not (empty? event)
        (let [e (first event)
              r (rest event)
              boat (:boat/id e)
              time (:time e)
              user (:user/id e)]
          (condp = (:user/activity e)
            :out (cond
                   (get-in agg [boat :out] false) (recur agg r (log-err-already-out err boat time))
                   :else (recur (register-out agg boat time user) r (log-normal-out err boat time)))
            :in (cond
                  (not (:out (get agg boat))) (recur agg r (log-err-never-out err boat time))
                  (nil? boat) (recur agg r (log-other err e time))
                  (nil? (get agg boat)) (recur agg r (log-other err e time))
                  :else (recur (register-in agg boat e) r (log-normal-in err boat time)))
            (recur agg r (log-unhandled err (:user/activity e) time))))
        {:data agg
         :log err}))))

(defn add-event [data time user-id activity boat-id]
  "events are aggregated into a sorted vector"
  (swap! data conj {:user/id user-id
                    :user/activity activity
                    :boat/id boat-id
                    :time time}))

(defn sample-events []
  (let [events (r/atom [])
        datas (demo-events)]
    (do
      (doseq [[user-id event-type boat-id] datas]
        (add-event events (now) user-id event-type boat-id))
      (reduce-events {} @events))))

(defn boats-currently-in-rent []
  (->> (:data (sample-events))
    ;(filter (fn [e] (:out (val e))))
       (map (fn [e] (key e)))
       (into #{})
       (set/intersection root-map-set)))

(defcard boats-out
  (boats-currently-in-rent))

(defcard boats-in
  (let [s (set/difference root-map-set (boats-currently-in-rent))
        c (count s)]
    {:boat-set s
     :count c}))

(defcard test-events
  (get (:data (sample-events)) 192))

(defcard test-events
  (sample-events))

(comment
  (def timestamp (t/date-time 2018 1 1 11 0 0))
  (let [init-state {}
        {d1 :data l1 :log} (reduce-events init-state
                                          [{:user/id 1
                                            :user/activity :out
                                            :boat/id 100
                                            :time (now)}
                                           {:user/id 2
                                            :user/activity :out
                                            :boat/id 200
                                            :time (now)}])
        {d2 :data l2 :log} (reduce-events d1
                                          [{:user/id 3
                                            :user/activity :out
                                            :boat/id 300
                                            :time (now)}
                                           {:user/id 1
                                            :user/activity :in
                                            :boat/id 101
                                            :time (now)}])
        {d3 :data l3 :log} (reduce-events d2
                                          [{:user/id 3
                                            :user/activity :out
                                            :boat/id 300
                                            :time (now)}
                                           {:user/id 1
                                            :user/activity :in
                                            :boat/id 101
                                            :time (now)}])]

    ;; think: d{n} is the model aggregated over time, the l{n}'s are just side-effects of this

    {:data d3 :log (concat l1 l2 l3)})

  (let [result {:data
                {100 {:out true, :usage 1, :time "08:27", :user/id 1},
                 200 {:out true, :usage 1, :time "08:27", :user/id 2},
                 300 {:out true, :usage 1, :time "08:27", :user/id 3}},
                :log
                '({:time "08:27:53.404", :ok "boat 100 marked out"}
                  {:time "08:27:53.404", :ok "boat 200 marked out"}
                  {:time "08:27:53.405", :ok "boat 300 marked out"}
                  {:time "08:27:53.405", :err "boat 101 was never out"}
                  {:time "08:27:53.406", :err "boat 300 was already out "}
                  {:time "08:27:53.406", :err "boat 101 was never out"})}]
    result))