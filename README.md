# NPanel - NRPK Panel

(development happens on the dev-branch)

MacOS:

Installer leiningen og noe som kompilerer SASS (eller lignende)

https://leiningen.org

```$ brew install sass/sass/sass```

Kjør følgende for å oppdatere ~/.m2 mappen din:

```$ lein deps```

---

Kjør følgende i prosjekt-rot i en separat prosess:

```$ sass --watch src/sass:resources/public/css```

Start IntelliJ (Community-Edition er godt nok) installert med Curisive og kjør `npanel devcards`-konfigurasjonen. IntelliJ vil åpne opp devcards i nettleseren din.


http://localhost:3449/cards.html

---
todo: Oppdater README.md med litt bedre instruks, og på engelsk?