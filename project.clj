(defproject npanel "0.1.0-NPanel"
  :cljfmt {:remove-consecutive-blank-lines? false}
  :description "FIXME: write description"
  :url "http://torv.be"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [ring-server "0.5.0"]
                 [reagent "0.8.1"]
                 [reagent-utils "0.3.1"]
                 [ring "1.6.3"]
                 [ring/ring-defaults "0.3.1"]
                 [compojure "1.6.1"]
                 [hiccup "1.0.5"]
                 [yogthos/config "1.1.1"]
                 [org.clojure/clojurescript "1.10.339" :scope "provided"]
                 [secretary "1.2.3"]
                 [com.andrewmcveigh/cljs-time "0.5.2"]
                 [venantius/accountant "0.2.4"
                  :exclusions [org.clojure/tools.reader]]
                 [figwheel-sidecar "0.5.16"]]

  :plugins [[lein-environ "1.1.0"]
            [lein-cljsbuild "1.1.7"]
            [lein-less "1.7.5"]
            [lein-sass "0.5.0"]
            [lein-asset-minifier "0.2.7"
             :exclusions [org.clojure/clojure]]]

  :ring {:handler npanel.handler/app
         :uberwar-name "npanel.war"}

  :min-lein-version "2.5.0"
  :uberjar-name "npanel.jar"
  :main npanel.server
  :clean-targets ^{:protect false} [:target-path
                                    [[:cljsbuild :builds :app :compiler :output-dir]
                                     [:cljsbuild :builds :app :compiler :output-to]]]

  :source-paths ["src/clj" "src/cljc" "script"]
  :resource-paths ["resources" "target/cljsbuild"]

  :minify-assets
  {:assets
   {"resources/public/css/site.min.css" "resources/public/css/site.css"}}

  :cljsbuild
  {:builds {:min
            {:source-paths ["src/cljs" "src/cljc" "env/prod/cljs"]
             :compiler
             {:output-to "target/cljsbuild/public/js/app.js"
              :output-dir "target/cljsbuild/public/js"
              :source-map "target/cljsbuild/public/js/app.js.map"
              :optimizations :advanced
              :pretty-print false}}

            :app
            {:source-paths ["src/cljs" "src/cljc" "env/dev/cljs"]
             :figwheel {:on-jsload "npanel.core/mount-root"}
             :compiler
             {:main "npanel.dev"
              :asset-path "/js/out"
              :output-to "target/cljsbuild/public/js/app.js"
              :output-dir "target/cljsbuild/public/js/out"
              :source-map true
              :optimizations :none
              :pretty-print true}}

            :test
            {:source-paths ["src/cljs" "src/cljc" "test/cljs"]
             :compiler {:main npanel.doo-runner
                        :asset-path "/js/out"
                        :output-to "target/test.js"
                        :output-dir "target/cljstest/public/js/out"
                        :optimizations :whitespace
                        :pretty-print true}}

            :devcards
            {:source-paths ["src/cljs" "src/cljc" "env/dev/cljs"]
             :figwheel {:devcards true
                        :open-urls ["http://localhost:3449/cards.html"]}
             :compiler {:main npanel.start
                        :asset-path "js/devcardsout"
                        :output-to "resources/public/js/app_devcards.js"
                        :output-dir "resources/public/js/devcardsout"
                        :source-map-timestamp true
                        :optimizations :none
                        :pretty-print true}}

            :hostedcards
            {:source-paths ["src/cljs" "env/dev/cljs"]
             :compiler {:main "npanel.start" ; <-- this one
                        :devcards true
                        :asset-path "js/hostedcards"
                        :output-to "resources/public/js/hostedcards.js"
                        :optimizations :advanced}}}}


  :doo {:build "test"
        :alias {:default [:chrome]}}

  :figwheel
  {:http-server-root "public"
   :server-port 3449
   :nrepl-port 7002
   :nrepl-middleware [cider.piggieback/wrap-cljs-repl
                      cider.nrepl/cider-middleware
                      refactor-nrepl.middleware/wrap-refactor]

   :css-dirs ["resources/public/css"]
   :ring-handler npanel.handler/app}


  :sass {:source-paths ["src/sass"]
         :target-path "resources/public/css"}

  :profiles {:dev {:repl-options {:init-ns npanel.repl}
                   :dependencies [[cider/piggieback "0.3.9"]
                                  [binaryage/devtools "0.9.10"]
                                  [ring/ring-mock "0.3.2"]
                                  [ring/ring-devel "1.6.3"]
                                  [lein-voom "0.1.0-20180617_140646-g0ba7ec8"]
                                  [prone "1.5.2"]
                                  [figwheel-sidecar "0.5.16"]
                                  [nrepl "0.4.5"]
                                  [org.clojure/test.check "0.10.0-alpha3"]
                                  [devcards "0.2.6" :exclusions [cljsjs/react]]
                                  ;;^{:voom {:repo "https://github.com/schpaa/devcards" :branch "urlencode-fix"}}
                                  [pjstadig/humane-test-output "0.8.3"]

                                  ;; To silence warnings from sass4clj dependecies about missing logger implementation
                                  [org.slf4j/slf4j-nop "1.7.25"]]


                   :source-paths ["env/dev/clj"]
                   ;; was: #_[lein-figwheel "0.5.16"]
                   :plugins [
                             [lein-doo "0.1.10"]
                             [cider/cider-nrepl "0.18.0"]
                             [org.clojure/tools.namespace "0.3.0-alpha4"
                              :exclusions [org.clojure/tools.reader]]
                             [refactor-nrepl "2.4.0-SNAPSHOT"
                              :exclusions [org.clojure/clojure]]
                             [deraen/lein-sass4clj "0.3.1"]]


                   :injections [(require 'pjstadig.humane-test-output)
                                (pjstadig.humane-test-output/activate!)]

                   :env {:dev true}}

             :uberjar {:hooks [minify-assets.plugin/hooks]
                       :source-paths ["env/prod/clj"]
                       :prep-tasks ["compile" ["cljsbuild" "once" "min"]]
                       :env {:production true}
                       :aot :all
                       :omit-source true}})
