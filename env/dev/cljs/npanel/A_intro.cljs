(ns npanel.A-intro
  (:require
   [clojure.string :refer [capitalize]]
   [clojure.pprint :refer [pprint]]
   [cljs.test :as t :include-macros true :refer-macros [testing is]]
   [cljs.spec.alpha :as s]

     ;;
   [reagent.core :as r :refer [atom]]
   [sablono.core :as sab]
   [devcards.core :as dc :refer [defcard-rg]]

     ;; time
   [cljs-time.core :as time :refer [minus]]
   [cljs-time.format :as f]
   [clojure.set :as set])

    ;; nrpk
    ;[nk.core]
    ;[nk.components.keypad :as keypad :refer [outer-just-keypad-component keypad-component two-step-keypad-component]]
    ;[nk.hoc.member :as member]
    ;[nk.hoc.statusbar :as statusbar]
    ;[nk.hoc.activity :as activity]
    ;[nk.hoc.rental :as rental]
    ;[nk.datalayer.member :as member-dl]
    ;[nk.datalayer.material :as material-dl]
    ;[cljs-time.core :as time]))

  (:require-macros
   [devcards.core :refer [defcard defcard-doc defcard-rg deftest mkdn-pprint-source]]))

(defcard-rg first
  (fn []
    [:div "Hello world"]))