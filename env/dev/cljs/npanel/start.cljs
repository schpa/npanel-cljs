(ns npanel.start
  (:require [npanel.A-intro]
            [npanel.B-panel-elements]
            [npanel.C-panel-elements])

  ;[reagent.core :as reagent :refer [atom]]))
  ;[nk.core :as core]))
  ;[sablono.core :as sab]
  ;[devcards.core :as dc]))
  ;[cljs.test :as t :include-macros true :refer-macros [testing is]]
  ;[cljs.spec.alpha :as s]
  ;[cljs-time.core]
  ;[nrpk.A-introduksjon]
  ;[nrpk.D-arshjul]
  ;[nrpk.F-nokkelvakt]
  ;[nrpk.C-materialbase]
  ;[nrpk.E-panel]
  ;[nrpk.F-panel]
  ;[nrpk.booking]
  ;[nrpk.E-innsjekking]
  ;[nk.core.event]
  ;[nrpk.B-concepts]))
  (:require-macros
   [devcards.core :refer [defcard defcard-doc defcard-rg deftest mkdn-pprint-source]]))
