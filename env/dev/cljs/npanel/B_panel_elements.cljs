(ns npanel.B-panel-elements
  (:require [devcards.core :as dc :refer [defcard-rg]]
            [npanel.data.material.boat.access]
            [npanel.html.components.boat.core :as boat-comp]
            [npanel.data.material.boat.core :as boat])
  (:require-macros [devcards.core :refer [defcard defcard-doc defcard-rg deftest mkdn-pprint-source]]))

(defcard-doc
  (dc/mkdn-pprint-source boat-comp/As-component))

(defcard-rg npanel.data.material.boat.core
  "old way"
  (fn []
    [(boat-comp/description-component "471")]))
  ;{}
  ;{:inspect-data true})


(defcard-rg npanel.data.material.boat.core
  (fn []
    (let [desc boat/sample-description]
      [(boat-comp/detailed desc)]))
  {}
  {:inspect-data true})

(defcard-rg npanel.data.material.boat.core
  (fn []
    (let [desc (boat/description-for "123")]
      [(boat-comp/detailed desc)]))
  {}
  {:inspect-data true})

(defcard-rg npanel.data.material.boat.core
  "detailed"
  (fn []
    (let [desc (boat/description-for "156")]
      ;[(boat-comp/description-component-v2 desc)]))
      (boat-comp/detailed desc)))
  {}
  {:inspect-data true})

(defcard-rg npanel.data.material.boat.core
  "mini"
  (fn []
    (let [desc (boat/description-for "156")]
      ;[(boat-comp/description-component-v2 desc)]))
      (boat-comp/minimal desc)))
  {}
  {:inspect-data true})