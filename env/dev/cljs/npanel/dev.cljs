(ns ^:figwheel-no-load npanel.dev
  (:require
    [npanel.core :as core]
    [devtools.core :as devtools]))

(devtools/install!)

(enable-console-print!)

(core/init!)
