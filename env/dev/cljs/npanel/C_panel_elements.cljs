(ns npanel.C-panel-elements
  (:require [devcards.core :as dc :refer [defcard-rg]]
            [cljs-time.core :refer [date-time]]
            [npanel.data.material.boat.access]
            [npanel.html.components.boat.core :as boat-comp]
            [npanel.data.material.boat.core :as boat]
            [npanel.html.components.panel.core :as panel]
            [npanel.reducer.core :as reducer]
            [reagent.core :as r])
  (:require-macros [devcards.core :refer [defcard defcard-doc defcard-rg deftest mkdn-pprint-source]]))

(defcard-doc
  (dc/mkdn-pprint-source boat-comp/As-component))

(def initial-model {})

(defn timestamp [minute] (date-time 2018 1 1 11 minute 0))

(def state (r/atom {:some/toggle 2
                    :main/view-state 1
                    :activity-view/sort-order 0
                    :activity (:data (reducer/reduce-events
                                      initial-model
                                       ;; events
                                      [{:user/id 1
                                        :user/activity :out
                                        :boat/id 100
                                        :time (timestamp 0)}
                                       {:user/id 2
                                        :user/activity :out
                                        :boat/id 200
                                        :time (timestamp 12)}
                                       {:user/id 3
                                        :user/activity :out
                                        :boat/id 300
                                        :time (timestamp 23)}
                                       {:user/id 3
                                        :user/activity :out
                                        :boat/id 400
                                        :time (timestamp 90)}
                                       {:user/id 2
                                        :user/activity :in
                                        :boat/id 200
                                        :time (timestamp 90)}]))}))

(defcard-rg statusbar
  (fn []
    (let [desc boat/sample-description]
      [panel/statusbar state]))
  state
  {:inspect-data true
   :watch-atom true})

(defcard-rg activity
  (fn []
    (let []
      [panel/activity state]))
  state
  {:inspect-data true
   :watch-atom true})
